import Vue from 'vue'
import appMain from "./appMain";
import appPreloader from "./assets/components/appPreloader"
import appNavbarMain from "./assets/components/appNavbarMain";
import appSlideMain from "./assets/components/appSlideMain";
import appFeaturesMain from "./assets/components/appFeaturesMain";
import appGalleryWorkMain from "./assets/components/appGalleryWorkMain";
import appLatestBlogMain from "./assets/components/appLatestBlogMain";
import appFooterMain from "./assets/components/appFooterMain";


Vue.component("appPreloader",appPreloader)
Vue.component("appNavbarMain",appNavbarMain)
Vue.component("appSliderMain",appSlideMain)
Vue.component("appFeaturesMain",appFeaturesMain)
Vue.component("appGalleryWorkMain",appGalleryWorkMain)
Vue.component("appLatestBlogMain",appLatestBlogMain)
Vue.component("appFooterMain",appFooterMain)

new Vue({
  el: '#appMain',
  render: h => h(appMain)
})
